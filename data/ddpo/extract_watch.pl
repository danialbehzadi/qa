#!/usr/bin/perl

use strict;
use warnings;
use DBD::Pg;
use DB_File;

my $dbh = DBI->connect("dbi:Pg:service=udd", '', '', {RaiseError => 1});
my $q = $dbh->prepare("SELECT source, status, warnings, upstream_version
	FROM (SELECT *, row_number() OVER (PARTITION BY source ORDER BY version DESC)
		FROM upstream
		WHERE status IS NOT NULL) sub
	WHERE row_number = 1");
$q->execute();

my $db_filename = 'watch-new.db';
my %db;
my $db_file = tie %db, "DB_File", $db_filename, O_RDWR|O_CREAT|O_TRUNC, 0666, $DB_BTREE
    or die "Can't open database $db_filename : $!";

while (my $pkg = $q->fetchrow_hashref) {
	my $p = $pkg->{source} || next;
	$db{"status:$p"} = $pkg->{status};
	$db{"warnings:$p"} = $pkg->{warnings};
	$db{"upstream-version:$p"} = $pkg->{"upstream_version"};
}
