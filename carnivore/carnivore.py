#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Utility functions to use carnivore
# Copyright (C) 2006, Jeroen van Wolffelaar<jeroen@wolffelaar.nl>
# Copyright (C) 2018, Asheesh Laroia<asheesh@asheesh.org>
# Copyright (C) 2023, Danial Behzadi<dani.behzi@ubuntu.com>
#
# This is polyglot code that should work properly in Python Python 3.10+

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from __future__ import absolute_import, division, print_function

from functools import reduce
import bsddb3


carnivore = bsddb3.btopen("/srv/qa.debian.org/carnivore/result.db", "r")


def join(sep, items):
    return reduce(lambda a, b: a + sep + b, items)


class carnivoreEntry:
    def __init__(self, vector):
        if not isinstance(vector, tuple):
            raise ValueError("Not a carnivore tuple")

        self.id = vector[0]
        (self.ldap, self.realname, self.gpg, self.email, self.package) = (
            [],
            [],
            [],
            [],
            [],
        )
        (self.extra, self.expl, self.warnings, self.mia) = [], [], [], []
        self.gecos = ""
        self.keyring = {
            "keyring": [],
            "emeritus": [],
            "removed": [],
            "ldap": [],
            "dm": [],
        }
        for item in set(vector[1] + vector[2]):
            if item.startswith("ldap:"):
                if item[5:] == "rhonda":  # "primary" uid, put in front
                    self.ldap.insert(0, item[5:])
                else:
                    self.ldap.append(item[5:])
            elif item.startswith("email:"):
                self.email.append(item[6:])
            elif item.startswith("realname:"):
                self.realname.append(item[9:])
            elif item.startswith("maint:"):
                self.package.append(item[6:])
            elif item.startswith("gpg:"):
                self.gpg.append(item[4:])
            elif item.startswith("x:"):
                self.expl.append(item[2:])
                if item.startswith("x:ldap:gpg:"):
                    self.keyring["ldap"].append(item[11:].split(":")[1])
                if item.startswith("x:gpg:keyring:"):
                    gpg, ring = item[14:].split(":")
                    self.keyring[ring].append(gpg)
                if item.startswith("x:ldap:realname:"):
                    self.gecos = item[16:].split(":")[1]
            else:
                self.extra.append(item)
        for ring in self.keyring.values():
            ring.sort()

    def __repr__(self):
        if self.ldap:
            return self.ldap[0]
        return self.email[0]

    def getPrettyprintedText(self):
        text = ""
        if self.ldap:
            text += f"DD: {self.gecos} <{self.ldap[0]}@debian.org>\n"
        if self.realname:
            text += f"Known as: {', '.join(self.realname)}\n"
        if self.email:
            text += f"Using emails: {', '.join(self.email)}\n"
        for key, value in self.keyring.items():
            for item in value:
                text += f"Key in {key}: {item}\n"
        i = self.ldap[0] if self.ldap else self.email[0]
        text += f"Links: https://qa.debian.org/developer.php?login={i} — "
        text += (
            f"https://contributors.debian.org/contributors/mia/query?q={i}\n"
        )
        if self.ldap:
            text += f"NM: https://nm.debian.org/person/{i}\n"
        else:
            text += "NM: https://contributors.debian.org/contributors/mia/"
            text += f"query?q={i}&to=nm\n"
        packs = "0"
        if self.package:
            packs = "{len(self.package)}"
            if len(self.package) <= 5:
                packs += f" ({', '.join(self.package)})"
            else:
                packs += f" ({', '.join(self.package[:4] + ['...'])})"
        text += f"Packages: %{packs}\n"
        if self.extra:
            for i in self.extra:
                text += i + "\n"

        return text


def search(key):
    if key not in carnivore:
        return []
    while True:
        lastkey = key
        key = carnivore[key]
        match key[0]:
            case "(":
                return [lastkey]
            case "[":
                return eval(key)


def get(key):
    while True:
        key = carnivore[key]
        match key[0]:
            case "(":
                return carnivoreEntry(eval(key))
            case "[":
                raise f"{key} is not an id of a real carnivore entry"


class iteritems(object):
    def __init__(self):
        self._iter = iter(carnivore.items())

    def iter(self):
        return self

    def next(self):
        while True:
            key, val = next(self._iter)
            if val[0] == "(":
                return key, carnivoreEntry(eval(val))


# vim: et
