CREATE TABLE popcon_day (
	day date PRIMARY KEY,
	submissions integer NOT NULL
);

CREATE TABLE popcon_architecture (
	architecture text,
	day date REFERENCES popcon_day (day),
	submissions integer NOT NULL,
	PRIMARY KEY (architecture, day)
);

CREATE TABLE popcon_release (
	release text,
	day date REFERENCES popcon_day (day),
	submissions integer NOT NULL,
	PRIMARY KEY (release, day)
);

CREATE TABLE popcon_vendor (
	vendor text,
	day date REFERENCES popcon_day (day),
	submissions integer NOT NULL,
	PRIMARY KEY (vendor, day)
);

-- package names are often long, create ids for them so the popcon table is smaller
CREATE TABLE popcon_package (
	id SERIAL PRIMARY KEY,
	package text NOT NULL,
	in_debian boolean NOT NULL DEFAULT false,
	-- most recent submission data:
	day date,
	vote integer,
	old integer,
	recent integer,
	no_files integer
);
CREATE UNIQUE INDEX ON popcon_package (package);

CREATE TABLE popcon (
	package_id integer NOT NULL REFERENCES popcon_package(id),
	day date NOT NULL REFERENCES popcon_day(day),
	vote integer NOT NULL,
	old integer NOT NULL,
	recent integer NOT NULL,
	no_files integer NOT NULL,
) PARTITION BY RANGE (day);

CREATE INDEX ON popcon (package_id);

GRANT SELECT ON popcon_day, popcon_architecture, popcon_release, popcon_vendor, popcon_package, popcon TO PUBLIC;

CLUSTER popcon_architecture USING popcon_architecture_pkey;
CLUSTER popcon_release USING popcon_release_pkey;
CLUSTER popcon_vendor USING popcon_vendor_pkey;
